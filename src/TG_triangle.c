#include "header.h"

int triangle_connection(int * X, int * Y, int * A, int * B, int * fn, int * tn, int v, int e, int nSample, int * tri){

    // initials parameter
    int tCount = 0 ;

    // algorithm
    for(int i = 0 ; i <  nSample; i++){
          if (X[i] == A[i]) {
              for(int j= 0; j < e ; j++){
                  if ((Y[i] == fn[j]) && (B[i] == tn[j] )){
                      tri[i]=1;
                      tCount+=1;
                      //printf("| - %d -->  %d  | %d --> %d \n", X[i], Y[i], A[i], B[i]);
                  }
              }
          }
    }
    return tCount;
}


float transitivityEstimation(int triangle, int nTripleSample){

    return  ((float)triangle/ (float)nTripleSample);
}


int triangleEstimation(float transEst, int nTripleSample){
    float a = (float) ((1.0/3.0)* (float)transEst);

    return (int) (a * (nTripleSample));
}
