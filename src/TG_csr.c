#include "header.h"

void CrE(int *fn, int *tn, int v, int e, int *col_idx, int *row_ptr, int *ncol_idx){

      // Parameter
      int ie = 0 ; // Index of Edges
      int irp = 0 ; // Index of Raw Prt
      int tmp_selisih;
      for(int i = 0 ; i< v; i++){
          ncol_idx[i]=0;
      }

      for(int i = 0; i < e; i++){
          if (fn[i] < tn[i]){
              col_idx[i] = tn[i];
              ie+=1;
              ncol_idx[tn[i]]+=1;

              if (i == 0 ){
                  row_ptr[irp] = 1 ;
                  irp+=1;
              }else if (i == (e-1)){
                  tmp_selisih = 0 ;
              }else{
                  tmp_selisih = fn[i+1] - fn[i];
              }
          // -------------------------------------------------
              if ((fn[i] != fn[i-1]) && ( tmp_selisih < 1 )) {
                  row_ptr[irp] = ie ;
                  irp+=1;
              }else if( (fn[i] != fn[i-1]) && (tmp_selisih >=1) ){
                  for(int j = 0 ; j < tmp_selisih; j++ ){
                      if (j==0){
                        row_ptr[irp]= ie ;
                        irp+=1;
                      }else{
                        row_ptr[irp]= ie ;
                        irp+=1;
                      }
                  }
              }else if(tmp_selisih > 1 ){
                  for(int j = 0 ; j < tmp_selisih; j++){
                    if (j >=1 ){
                      row_ptr[irp]= ie+1 ;
                      irp+=1;
                    }
                  }
              }
          }
      }
}

int CoD(int * ncol_idx, int * row_ptr, int v, int e, int *de){

      int nde = 0 ; // Number of Degree

      for(int i = 0 ; i<v ; i ++){
          if (i==0){
              de[i]= row_ptr[i+1] - row_ptr[i];
              nde = nde + de[i];
          }else if( (i != 0 ) && (i < (v-2)) ){
              if ( row_ptr[i] != 0 ){
                  de[i] = (ncol_idx[i]) + (row_ptr[i+1] - row_ptr[i]) ;
              }else if(row_ptr[i] ==  0){
                  de[i] = ncol_idx[i];
              }
              nde = nde + de[i];
          }else if( i == (v-2)) {
              if(row_ptr[i]== 0 ){
                  de[i] = ncol_idx[i];
              }else if(row_ptr[i] != 0 ){
                  de[i] = (ncol_idx[i]) + ( (row_ptr[i] + 1 ) - row_ptr[i] );
              }
              nde = nde + de[i];
          }else{
              de[i] = ncol_idx[v-1];
              nde = nde + de[i];
          }
      }
      return nde;
}
