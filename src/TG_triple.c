#include "header.h"

int triple_counting(int * de, int v, int e){
   int nTriple = 0 ;
   for(int i = 0 ; i < v ; i ++ ){
       nTriple += ((de[i] * (de[i]-1)) / 2);
   }
   return nTriple;
}

int numberSampling(int nTriple, float sampleRate){
  float float_rate;
  int int_rate;
  int raidx, rbidx ;
  float_rate =  (nTriple * sampleRate);
  int_rate = (int) float_rate;
  return int_rate;
}


int getTripleSampling( char projectname[], int nSampling, int v, int e, int * fn, int * tn ){

    // initials Parameter
    int nSampling_check = 0 ;
    int X,Y,A,B;

    char Path[256];
    strcpy(Path, "output/");
    strcat(Path, projectname);
    strcat(Path, "_");
    strcat(Path, "tripleSample.txt");


    FILE *exportFile = fopen(Path, "w");
    int raidx, rbidx;

    // algorithm

    while(nSampling_check <  nSampling){
        for(int j = 0; j < e; j++){
            // Random indexing
            raidx = rand() % (e); // random for coordinate 1
            rbidx = rand() % (e); // random for coordinate 2

            // set double coordinate with (X,Y) and (A,B)
            X = fn[raidx];
            Y = tn[raidx];
            A = fn[rbidx];
            B = tn[rbidx];

            // Search Triple
            if((X==A) && (Y!=B) && (Y < B)) {
                // Write to file
                fprintf(exportFile, "%d\t%d\t%d\t%d\n",  X, Y, A , B);
                nSampling_check+=1;
            }else if((X==B) && (Y!=A) && (Y < A)){
                fprintf(exportFile, "%d\t%d\t%d\t%d\n",  X, Y, A , B);
                nSampling_check+=1;
            }else if((X!=A) && (Y==B) && (X < A)){
                fprintf(exportFile, "%d\t%d\t%d\t%d\n",  Y, X, B , A);
                nSampling_check+=1;
            }else if((X!=B) && (Y==A) && (X < B)){
                fprintf(exportFile, "%d\t%d\t%d\t%d\n",  Y, X, A , B);
                nSampling_check+=1;
            }
        }
    }
    fclose(exportFile);
    printf("total triple by check connection %d \n", nSampling_check);

    return nSampling_check;
}
