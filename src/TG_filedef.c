#include "header.h"


void loadData(FILE** importFile ,char fileName[]){
      *importFile = fopen(fileName, "r");
      if (*importFile != NULL){
        printf("File Successfully opened a. \n");
      }
}

void WtoF(char fileName[], char projectname[]  , int * mat1d, int n){
      char Path[256];
      strcpy(Path, "output/");
      strcat(Path, projectname);
      strcat(Path, "_");
      strcat(Path, fileName);


      FILE *exportFile= fopen(Path, "w");
      for(int i = 0 ; i < n ; i++){
          fprintf(exportFile, "%d\n", mat1d[i]);
      }
      fclose(exportFile);

}

void grahpExport(FILE *importFile, int edges, int *fn, int *tn){
      int number, i;
      if (importFile == NULL){
          printf("File not exits. \n");
      }else{
       for(i = 0 ; i < edges ; i++ ){
         if(!fscanf(importFile, "%d %d", &fn[i], &tn[i] )){
           break;
         }
       }
      }
      fclose(importFile);
 }

 void deR(FILE *importFile, int *de, int v){

    if(importFile == NULL){
      printf(" File not Exist");
    }else{
        for(int i = 0 ; i < v  ; i++){
            if(!fscanf(importFile, "%d", &de[i])){

                break;
            }
        }
    }
    fclose(importFile);
 }


 void getTripleConnection(char fileName[], char projectname[], int nSample, int *X, int *Y, int *A, int *B){

   // path
   FILE *importFile;
   char Path[256];
   strcpy(Path, "output/");
   strcat(Path, projectname);
   strcat(Path, "_");
   strcat(Path, fileName);

   loadData(&importFile, Path);

   if (importFile == NULL){
     printf("File not exits. \n");
   }else{
     for(int i = 0 ; i < nSample ; i++ ){
       if(!fscanf(importFile, "%d\t%d\t%d\t%d", &X[i], &Y[i], &A[i], &B[i]  )){
         break;
       }
     }
   }
   fclose(importFile);
 }


 void WtriangleToFile(int * X, int * Y, int * A, int * B, int * tri, int nSample, char fileName[], char projectname[]){

    // Path
    char Path[256];
    strcpy(Path, "output/");
    strcat(Path, projectname);
    strcat(Path, "_");
    strcat(Path, fileName);


     FILE *exportFile = fopen(Path, "w");
     for(int i = 0 ; i < nSample; i++){
         fprintf(exportFile, "%d\t%d\t%d\t%d\t%d\n",  X[i], Y[i], A[i], B[i], tri[i]);
     }
     fclose(exportFile);
 }


 void Zeros1DM(int * arr, int number){

     for(int i = 0 ; i < number; i ++ ){
         arr[i] = 0;
     }
 }
