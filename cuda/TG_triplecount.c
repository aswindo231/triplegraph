#include "tgcuda.h"

__global__ void tripleCounting(int nTriple, int *de, int v)
{

    int i = threadIdx.x;

    if(i < v){
        nTriple = nTriple + (((de[i] * (de[i]-1)) / 2)) ;
    }
}
