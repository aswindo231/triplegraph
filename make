#!/bin/bash

gcc -c -I /home/user/aslab-03/project/graph/include /home/user/aslab-03/project/graph/src/TG_filedef.c
gcc -c -I /home/user/aslab-03/project/graph/include /home/user/aslab-03/project/graph/src/TG_csr.c
gcc -c -I /home/user/aslab-03/project/graph/include /home/user/aslab-03/project/graph/src/TG_triple.c
gcc -c -I /home/user/aslab-03/project/graph/include /home/user/aslab-03/project/graph/src/agrparse.c
gcc -c -I /home/user/aslab-03/project/graph/include /home/user/aslab-03/project/graph/src/TG_triangle.c
gcc -c -I /home/user/aslab-03/project/graph/include /home/user/aslab-03/project/graph/src/TG_main.c

gcc -o main  TG_filedef.o TG_csr.o TG_triple.o TG_triangle.o agrparse.o TG_main.o

resd
