#ifndef HEADER_H
#define HEADER_H


// System Include
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <string.h>
#include "argparse.h"
#include <time.h>

static const char *const usage[] = {
    "test_argparse [options] [[--] args]",
    "test_argparse [options]",
    NULL,
};

#define PERM_READ  (1<<0)
#define PERM_WRITE (1<<1)
#define PERM_EXEC  (1<<2)

// global variable



void argumentparsefile(int argc, const char **argv);

//-------------------- List of header file defenition function ----------------- //

void loadData(FILE** importFile ,char fileName[]);
void WtoF(char fileName[], char projectname[]  , int * mat1d, int n);
void grahpExport(FILE *importFile, int edges, int *fn, int *tn);
void deR(FILE *importFile, int *de, int v);
void getTripleConnection(char fileName[], char projectname[], int nSample, int *X, int *Y, int *A, int *B);
void WtriangleToFile(int * X, int * Y, int * A, int * B, int * tri, int nSample, char fileName[], char projectname[]);
void Zeros1DM(int * arr, int number);

// --------------------list of header csr method  --------------------------//
void CrE(int *fn, int *tn, int v, int e, int *col_idx, int *row_ptr, int *ncol_idx);
int CoD(int * ncol_idx, int * row_ptr, int v, int e, int *de);


// triple_counting
int triple_counting(int * de, int v, int e);
int numberSampling(int nTriple, float sampleRate);
int getTripleSampling( char projectname[], int nSampling, int v, int e, int * fn, int * tn );

//trieangle
int triangle_connection(int * X, int * Y, int * A, int * B, int * fn, int * tn, int v, int e, int nSample, int * tri);
float transitivityEstimation(int triangle, int nTripleSample);
int triangleEstimation(float transEst, int nTripleSample);


#endif
